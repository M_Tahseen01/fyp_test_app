import 'package:fyp_test_app/models/brew.dart';
import 'package:fyp_test_app/screens/home/brew_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BrewList extends StatefulWidget {
  @override
  _BrewListState createState() => _BrewListState();
}

class _BrewListState extends State<BrewList> {
  @override
  Widget build(BuildContext context) {
    final userinfo = Provider.of<List<Brew>>(context) ?? [];

    return ListView.builder(
      itemCount: userinfo.length,
      itemBuilder: (context, index) {
        return BrewTile(brew: userinfo[index]);
      },
    );
  }
}
